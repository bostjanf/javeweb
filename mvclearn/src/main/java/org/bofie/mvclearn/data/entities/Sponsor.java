package org.bofie.mvclearn.data.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Sponsor {
	
	private String name;

	private String phone;

	private String email;

}
