package org.bofie.mvclearn.data.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class Project {

    private Long projectId;

    private String name;

    @NotBlank(message="You must provide a description")
    private String description;

    private Sponsor sponsor;

    private BigDecimal authorizedHours;

    private BigDecimal authorizedFunds;

    private String year;

    private boolean special;

    private String type;

    private List<String> pointsOfContact;
}
