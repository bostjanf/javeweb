package org.bofie.mvclearn.data.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Resource {
    private Long resourceId;

    private String name;

    private String type;

    private String[] indicators;

    private BigDecimal cost;

    private String unitOfMeasure;

    private String notes;
}
