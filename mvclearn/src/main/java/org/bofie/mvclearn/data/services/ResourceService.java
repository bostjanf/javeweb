package org.bofie.mvclearn.data.services;

import org.bofie.mvclearn.data.entities.Resource;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResourceService {

	private List<Resource> resources = new LinkedList<Resource>();

	public ResourceService() {
		this.resources.add(new Resource(1L, "Coder", "Staff",   null, new BigDecimal("100.00"), "Hours", null));
		this.resources.add(new Resource(1L, "Analyst", "Staff", null,  new BigDecimal("50.00"), "Hours", null));
		this.resources.add(new Resource(1L, "Tester", "Staff", null,  new BigDecimal("70.00"), "Hours", null));
	}

	public List<Resource> findAll() {
		return this.resources;
	}

	public Resource find(Long resourceId) {
		return this.resources.stream().filter(r -> {
			return r.getResourceId().equals(resourceId);
		}).collect(Collectors.toList()).get(0);
	}
}
