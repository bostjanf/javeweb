package org.bofie.mvclearn.controllers;

import java.util.ArrayList;

import javax.validation.Valid;

import org.bofie.mvclearn.HitCounter;
import org.bofie.mvclearn.data.entities.Project;
import org.bofie.mvclearn.data.services.ProjectService;
import org.bofie.mvclearn.data.validators.ProjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/project")
public class ProjectController {

	@Autowired
	public ProjectService projectService;

	@Autowired
	private HitCounter hitCounter;

	@RequestMapping(value="/{projectId}")
	public String findProject(Model model, @PathVariable Long projectId) {
		model.addAttribute("project", this.projectService.find(projectId));
		return "project";
	}

	@ResponseBody
	@RequestMapping(value="/api/{projectId}")
	public Project findProjectObject(@PathVariable Long projectId) {
		return this.projectService.find(projectId);
	}
	
	@RequestMapping(value="/find")
	public String find(Model model) {
		model.addAttribute("projects", this.projectService.findAll());
		return "projects";
	}
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String addProject(Model model){

		hitCounter.setHits(hitCounter.getHits()+1);
		System.out.println("The number og hits is :"+hitCounter.getHits());
		model.addAttribute("types", new ArrayList<String>(){{
			add("");
			add("Single Year");
			add("Multi Year");
		}});
		
		model.addAttribute("project", new Project());

		return "project_add";
	}

	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String saveProject(@Valid @ModelAttribute Project project, Errors errors, RedirectAttributes attributes) {
		System.out.println("invoking save project");



		project.setProjectId(55L);
		this.projectService.save(project);
		attributes.addFlashAttribute("project", project);
				
		return "redirect:/home/";
	}

}
