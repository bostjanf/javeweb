package org.bofie.mvclearn.controllers;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.bofie.mvclearn.data.entities.Resource;
import org.bofie.mvclearn.data.services.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;


@Controller
@RequestMapping("/resource")
@SessionAttributes("resource")
public class ResourceController {

	@Autowired
	private ResourceService resourceService;


	@RequestMapping(value = "/{resourceId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Resource findResource(@PathVariable("resourceId") Long resourceId){
		return resourceService.find(resourceId);
	}

	@RequestMapping("/find")
	public String find(Model model){
		model.addAttribute("resources",resourceService.findAll() );
		return "resources";
	}

	@RequestMapping("/add")
	public String add(Model model) {
		System.out.println("Invoking the add method");

		if(1 == 1) {
			throw new RuntimeException("This is an exception");
		}
		
		return "resource_add";
	}
	

	@ResponseBody
	@RequestMapping("/request")
	public String request(@RequestBody String resource) {
		System.out.println(resource);
		//Send out an email
		return "The request has been sent for approval";
	}
	
	@RequestMapping("/review")
	public String review(@ModelAttribute Resource resource) {
		System.out.println("Invoking the review method.");
		return "resource_review";
	}
	
	@RequestMapping("/save")
	public String save(@ModelAttribute Resource resource, SessionStatus status) {
		System.out.println(resource);
		System.out.println("Invoking the save method");
		status.setComplete();
		return "redirect:/resource/add";
	}

	@ModelAttribute("resource")
	public Resource getResource() {
		System.out.println("Adding an attribute to the model");
		return new Resource(); 
	}
	
	@ModelAttribute(value = "checkOptions")
	public List<String> getChecks() {
		return new LinkedList<>(Arrays.asList(new String[] { "Lead Time", "Special Rate", "Requires Approval" }));
	}

	@ModelAttribute(value = "radioOptions")
	public List<String> getRadios() {
		return new LinkedList<>(Arrays.asList(new String[] { "Hours", "Piece", "Tons" }));
	}
	
	@ModelAttribute(value = "typeOptions")
	public List<String> getTypes() {
		return new LinkedList<>(
				Arrays.asList(new String[] { "Material", "Staff", "Other", "Equipment" }));
	}
	
}
