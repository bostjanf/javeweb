package org.bofie.mvclearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class MvclearnApplication  extends SpringBootServletInitializer {

    public static void main(String[] args)
    {
        SpringApplication.run(MvclearnApplication.class, args);
    }
}
